import { Tree } from "./Tree/Tree.js";
//=========================================================
export class SeedClient extends Tree {
//=========================================================

    //-----------------------------------------------------
    constructor(entry_point) {
    //-----------------------------------------------------
        super(null,null);
        this.entry_point = entry_point;
        this.selected="/";
    }
    //-----------------------------------------------------
    ask_json(formData,callback,method="POST") {
    //-----------------------------------------------------
        formData["select"]=this.selected;
        var app=this;
        fetch(this.entry_point, {
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
                method: method,
                body: JSON.stringify(formData)
        }).then(response => response.json() )
        .then(data  => callback(data) )
      .catch(error => console.error(error));
    }
    //-----------------------------------------------------
    ask_text(formData,callback,method="GET") {
    //-----------------------------------------------------
        var app=this;
        fetch(this.entry_point, {
              method: method,
              body: formData
        }).then(response => response.text() )
        .then(data  => callback(data) )
          .catch(error => console.error(error));
    }
    //-----------------------------------------------------

}
//=========================================================
export class Module extends Tree {
//=========================================================

    //-----------------------------------------------------
    constructor(client,id) {
    //-----------------------------------------------------
        super(client,id);
        this.id = id;

    }
    //-----------------------------------------------------
    update() {
    //-----------------------------------------------------
       console.log("update",this.id);
    }
    //-----------------------------------------------------

}
//=========================================================


//$("#test").text("ok");





