
//========================================================

class Div extends SceneElement {

//========================================================

    //----------------------------------------------------
    onSetup(){
    //----------------------------------------------------
        this.html=null;
        this.add_variable("active",false);
        this.add_variable("balise","div");
        this.add_variable("recursive_event",false);
        this.args={};
        var cls=this.constructor.name;
        if(this.__data["class"] ) {
            this.args={class:cls+" "+this.__data["class"],};
        }else{
            this.args={class:cls,};
        }


    }
    //----------------------------------------------------
    draw(div,selected){
    //----------------------------------------------------

        this.html= super.draw(div,selected);
        if( this.active){
            this.activate(this.html,selected);
        }
        return this.html;
    }

    //----------------------------------------------------
    onDraw(div,selected){
    //----------------------------------------------------

      return this.get_html(this.balise,this.args,null,div);

    }
    //----------------------------------------------------

    activate(div,selected){

    //----------------------------------------------------
        //console.log("activate",this.path(),selected);
        this.children.by_class(Do).by_attr_value("event","init").execute("call",{div:div,selected:selected});
        div.on("click",{div:div,selected:selected},this.onClick.bind(this));
        div.on("mouseenter",{div:div,selected:selected},this.onMouseEnter.bind(this));
        div.on("mouseleave",{div:div,selected:selected},this.onMouseLeave.bind(this));
     }
    //----------------------------------------------------

    onClick(event){
    //----------------------------------------------------
        this.msg("ON_CLICK",event.data,5);
        this.executeEvent("click",this.onClickDone,event.data,this.recursive_event);
    }
    //----------------------------------------------------

    onMouseEnter(event){
    //----------------------------------------------------
        this.msg("ON_MOUSE_ENTER",event.data,5);
        this.executeEvent("enter",this.onMouseEnterDone,event.data,this.recursive_event);
    }
    //----------------------------------------------------

    onMouseLeave(event){
    //----------------------------------------------------
        this.msg("ON_MOUSE_LEAVE",event.data,5);
        this.executeEvent("leave",this.onMouseLeaveDone,event.data,this.recursive_event);
    }
    //----------------------------------------------------

    onClickDone(event){}
    onMouseEnterDone(event){}
    onMouseLeaveDone(event){}
    //----------------------------------------------------

}
//========================================================

