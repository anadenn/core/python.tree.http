//========================================================

class Layer extends Div {

//========================================================
    //----------------------------------------------------
    onSetup(){
    //----------------------------------------------------

        super.onSetup();
        this.add_variable("url",null);
        this.args["class"]=this.args["class"]+" w3-display-topleft";//fullsize

    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------
        this.msg("LAYER_SELECT",selected,5);

        var url;
        if("url" in this){
            url=this.url;

        }else{
            url=selected.url;
        }
        this.obj=this.get_html("div",this.args,null,div);
        var img=jQuery('<img/>',{class:"w3-image",src:url});
        img.appendTo(this.obj);
        //img.css("position","absolute");
        this.obj.css("width","100%");
        //img.css("top","0");
        //img.css("left","0");
        //img.css("float","right");
        return this.obj;

   }

    //----------------------------------------------------

    onDrawElementXX(div,selected){

    //----------------------------------------------------
        this.obj=super.onDrawElement(div,selected);
        img=jQuery('<img/>',{class:"w3-image",src:this.get_url(selected)});
        img.appendTo(this.obj);
        return this.obj;

   }
    //----------------------------------------------------

}
//========================================================

