//========================================================

class Scene extends SceneNode {

//========================================================

    //----------------------------------------------------
    onCall(data){

    //----------------------------------------------------
            this.draw(data.div,data.selected);
    }
    //----------------------------------------------------
    onDraw(div,selected){

    //----------------------------------------------------
        return div;
    }
    //----------------------------------------------------
    drawScene(name,div,selected){
    //----------------------------------------------------
        if(this.parent){
            var node=this.parent.search_path(name);
            if(node){
                //console.log("DRAWSCENE",name);
                this.msg("DRAWSCENE",name,5);
                //console.log("DRAWSCENE CLOSE",name);
                return node.draw(div,selected);
            }else{
                //console.log("draw not found",name,selected,div);
            }
        }else{
                //return this.draw(div,selected);
        }

    }
    //----------------------------------------------------

}

//========================================================
