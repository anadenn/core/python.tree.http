//========================================================

class SelectXml extends SceneElement {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("target",null);
    }
    //----------------------------------------------------
    draw(div,selected){
    //----------------------------------------------------
        var node=null;

        if(this.target!=null){
            node=selected.search_path(this.target);
        }else{
            node=selected;
        }
        if(node!=null){
            this.LoadFile(this.get_url(node),this.onFileLoaded.bind(this),"obj",null,{div:div,selected:node});
        }
    }
    //----------------------------------------------------
    onFileLoaded(data){
    //----------------------------------------------------
        //console.log(data);
        //data.response.attach(data.args.node);
        data.response.setup();
        super.draw(data.div,data.response);

    }
    //----------------------------------------------------

}
//========================================================

