//========================================================

class CallFromData extends SceneElement {

//========================================================
    // appeler une scene stockée dans Bd_Data.url
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        this.add_variable("part",null);
        this.add_variable("select");
    }

    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------
        var parent;
        if(this.part){
            var parent=$("#"+this.part);
        }else{
            var parent=div;
        }
        if(this.select){
            var node=this.parent.search_path(this.select);
        }else{
            var node=selected;
        }

        parent.empty();
        //console.log("Call_Scene",this.scene,selected.name,parent);
        this.drawScene(selected.url,parent,node);

    }
    //----------------------------------------------------
}
//========================================================

