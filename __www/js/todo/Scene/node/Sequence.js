
        
//========================================================

class Sequence extends SceneElement {

//========================================================

    //----------------------------------------------------
    onSetup(){
    //----------------------------------------------------

        super.onSetup();
        this.add_variable("dt",1.0);
        this.add_variable("empty",false);

    }

    //----------------------------------------------------
    draw(div,selected){

    //----------------------------------------------------
        this.msg("SEQ_START",{},5);
        this.OnDrawNext(div,selected,0) ;
  
    }
    //----------------------------------------------------

    cycle_stop() {

    //----------------------------------------------------

        clearInterval(this.interval);

    }
    //----------------------------------------------------

    OnDrawNext(div,selected,counter) {

    //----------------------------------------------------

        if(this.interval){
            this.cycle_stop();
        }

        if(this.empty){
            div.empty();
        }


        var lst=this.children.by_class(SceneElement).sorted();


        if(counter<lst.length){
            this.msg("SEQ_DRAW",counter,6);
            var node=lst[counter]
            node.draw(div,selected);
            counter+=1;

            this.interval = setInterval(this.OnDrawNext.bind(this), this.dt*1000,div,selected,counter);
        }else{
            this.msg("SEQ_STOP",counter,6);
        }
    }
    //----------------------------------------------------


}
//========================================================




