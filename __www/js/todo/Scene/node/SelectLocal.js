//========================================================

class SelectLocal extends SceneElement {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("value");
    }
    //----------------------------------------------------
    draw(div,selected){

    //----------------------------------------------------
        var node=this.search_path(this.value);

        if(node!=null){
            super.draw(div,node);
        }
    }
    //----------------------------------------------------

}

//========================================================

