//========================================================

class SceneNode extends ScriptNode {

//========================================================

    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.url=null;
    }
    //----------------------------------------------------

    drawScene(name,div,selected){

    //----------------------------------------------------
    this.msg("DRAWSCENE",name,5);
    return this.parent.drawScene(name,div,selected);

    }
    //----------------------------------------------------

    draw(div,selected){

    //----------------------------------------------------
        this.msg("DRAW",{div:div,selected:selected},4);

        div=this.onDraw(div,selected);
        this.children.by_class(SceneElement).execute("draw",div,selected);
        this.onDrawDone(div,selected);
        //this.msg("DRAW_DONE",{div:div,selected:selected},5);
        return div;
    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------
        //renvoie la div pour afficher les SceneElement enfants
        return div;

    }
    //----------------------------------------------------
    onDrawDone(div,selected){
    //----------------------------------------------------
        //renvoie la div pour afficher les SceneElement enfants
        return div;

    }
    //----------------------------------------------------

    get_html(name,args,tag=null,parent=null,text=null){

    //----------------------------------------------------
        var local_arguments=null;

        if(args!=null){
            local_arguments=args;
        }else{
            local_arguments={};
        }

        if(tag){
            local_arguments["id"]=tag;
        }

        var self=jQuery('<'+name+'/>',local_arguments);
        if(text){self.append(text);}

        if(parent){
            self.appendTo(parent);
        }

        return self;
    }
    //----------------------------------------------------

    get_url2(selected){

    //----------------------------------------------------
            return selected.url;
    }
    //----------------------------------------------------

    get_url(selected){

    //----------------------------------------------------
        var url=null;

        if(this.url){
            url= this.url;
        }else{
            url= selected.url;
        }

        //console.log("    URL",url,selected);
        if(url.startsWith('http')){
            //url= url;

        } else if(url.startsWith('/')|| url.startsWith('.')){
            //url= url;
        }else{
            //url= this.root().url+url;
        }
//console.log(url);

        return url;
    }
    //----------------------------------------------------

}
//========================================================
