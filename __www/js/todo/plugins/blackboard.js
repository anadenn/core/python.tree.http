
//========================================================

class Blackboard_ClsHtml extends Bd_Plottable {

//========================================================
    //----------------------------------------------------
    constructor(parent,xmlnode){
    //----------------------------------------------------

        super(parent,xmlnode);

    }
    //----------------------------------------------------

    onRedraw(node){
        var url="http://0.0.0.0:8000/api/templates/classes?path="+node.filepath;
        this.div.load(url);
    }
    //----------------------------------------------------
}
//========================================================


class Blackboard_Xml extends Bd_Plottable {

//========================================================
    //----------------------------------------------------
    constructor(parent,xmlnode){
    //----------------------------------------------------

        super(parent,xmlnode);
        this.template=xmlnode.attributes["template"].value;
    }
    //----------------------------------------------------

    onRedraw(node){
        var url="http://0.0.0.0:8000/api/templates/get?template="+this.template+"&node="+node.filepath;
        new Query(this,url,this.onLoadFile,null,"xml");
    }
    //----------------------------------------------------

    onLoadFile(data){

    //----------------------------------------------------
        //console.log(data);
        this.data=process_node(this,data);
        //this.data.print();
        //console.log(data);
        this.data.setup();

    }
    //----------------------------------------------------

}
//========================================================

class Template extends Page {

//========================================================

    //----------------------------------------------------

    get_url(){

    //----------------------------------------------------
        return "http://0.0.0.0:8000/api/templates/get?template="+this.url+"&node="+this.selected_node;

    }
    //----------------------------------------------------


}
//========================================================

class TemplateXml extends Page {

//========================================================

    //----------------------------------------------------

    draw(parent,node){

    //----------------------------------------------------
        //this.children.execute("destroy");
        var url="http://0.0.0.0:8000/api/templates/get?template="+this.url+"&node="+node;
        new Query(this,url,this.onLoadFile.bind(this),null,"xml");
        this.selected_node=node;
        this.parent_node=parent;
    }
    //----------------------------------------------------

    onLoadFile(data){

    //----------------------------------------------------
        //console.log(data);
        //if(this.data){
        //    this.data.destroy();
        //}
        //console.log(this,data);
        this.data=process_node(this,data);
        //this.data.print();
        //console.log(data);
        this.data.setup();
        this.data.draw(this.parent_node,this.selected_node);
    }
    //----------------------------------------------------

}
//========================================================
//========================================================
