//========================================================

class PlayerSequence{

//========================================================
    //----------------------------------------------------
    constructor(){
    //----------------------------------------------------
        this.sequence=[];
        this.i=-1;
        this.makelist();
    }
    //----------------------------------------------------
    makelist(){
    //----------------------------------------------------
        return;
    }
    //----------------------------------------------------
    getNode(){
    //----------------------------------------------------
        return this.sequence[this.i];
    }
    //----------------------------------------------------
    addNode(node){
    //----------------------------------------------------
        this.sequence.push(node);
    }
    //----------------------------------------------------
    reset(){
    //----------------------------------------------------

        this.i=-1;

    }

    //----------------------------------------------------
    next(n=1){
    //----------------------------------------------------
        this.i+=n;
        if(this.i>=this.sequence.length){this.i=0;}
        return this.getNode();

    }
    //----------------------------------------------------
    previous(n=1){
    //----------------------------------------------------
        this.i-=n;
        if(this.i<0){this.i=this.sequence.length-1;}
        return this.getNode();
    }
    //----------------------------------------------------

}

//========================================================

class PlayerHandler extends Bd_Object {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        super.onSetup(data);
        this.add_variable("attr");
        this.add_variable("value");
        this.add_variable("scene");
        this.add_variable("tag");

    }
    //----------------------------------------------------
    test(node){
    //----------------------------------------------------

        if(node[this.attr]==this.value){
            return true;
        }
        return false;
    }
    //----------------------------------------------------
    process(node){
    //----------------------------------------------------

        var scene = this.search_path(this.scene);
        $("#"+this.tag).empty();
        scene.draw($("#"+this.tag),node);
    }
    //----------------------------------------------------

}
//========================================================

class Player extends Bd_Object {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        super.onSetup(data);
        this.add_variable("t_loop",8000);
        this.add_variable("select");
        this.sequence=new PlayerSequence();
        this.selectors=this.children.by_class(PlayerHandler).iter();


        this.current_node=null;
        this.paused=true;


    }
    //----------------------------------------------------
    post_setup(){
    //----------------------------------------------------
        var node;
        var select=this.search_path(this.select);

        if(! select ){
            //console.log("select not exists",this.select);
            return;
        }
        var elements=select.all().iter();
        for(var i = 0, size = elements.length; i < size ; i++){
             node=elements[i];
             if (this.get_selector(node)!=false ) {
                //console.log(node.path());
                this.sequence.addNode(node);
            }
        }

        //console.log(this.sequence.sequence.length);
    }
    //----------------------------------------------------
    get_selector(node){
    //----------------------------------------------------

        for(var i = 0, size = this.selectors.length; i < size ; i++){
             if (this.selectors[i].test(node) == true ) {
                return this.selectors[i];
            }
        }
        return false;

    }
    //----------------------------------------------------
    process(node){
    //----------------------------------------------------
        //console.log(this.sequence.i,node.path());
        this.current_node=node;
        var selector=this.get_selector(node);
        if(selector){
            selector.process(node);
        }
    }

    //----------------------------------------------------
    reset(){
    //----------------------------------------------------
        this.sequence.reset();
        this.next();
    }
    //----------------------------------------------------
    start(){
    //----------------------------------------------------

        if(this.paused){
            this.paused=false;
            this.loop();
        }
    }
    //----------------------------------------------------
    stop(){
    //----------------------------------------------------

        this.paused=true;
    }
    //----------------------------------------------------
    loop(){
    //----------------------------------------------------

        if(this.paused==false){
            this.next();
            setTimeout(this.loop.bind(this), this.t_loop);
        }
    }
    //----------------------------------------------------
    next(n=1){
    //----------------------------------------------------
        this.process(this.sequence.next(n));

    }
    //----------------------------------------------------
    previous(n=1){
    //----------------------------------------------------
        this.process(this.sequence.previous(n));
    }
    //----------------------------------------------------

}
//========================================================
