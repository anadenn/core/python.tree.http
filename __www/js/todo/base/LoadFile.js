
        
//========================================================

class LoadFile extends Task {

//========================================================
    /*
    object to handle query
    with an optional output format option.

    list of formats :
    * json
    * xml
    * obj -> creer les objets js
    
    basic usage returns text:
        var q=new Query(url,callback);

    usage:
        var q=new Query(url,callback,format);

    cyclic behavior, dt in millis:
        var q=new Query(url,callback,format,dt);

    callback function :
        function callback(data){
            ...
        }
    */

    //----------------------------------------------------
    constructor(parent,callback,url,format=null,dt=null,data=null){
    //----------------------------------------------------

        super(parent,callback,data);

        //attributes
        this.url=url;
        this.dt=dt;
        this.format=format;

        //request
        this.request = new XMLHttpRequest();
        this.response;
        this.interval;
    }

    //----------------------------------------------------
    onStart(){
    //----------------------------------------------------


        //query
        if ( this.dt !=null) {
            this.cycle_start();

        }else{
            this.query();
        }

    }

    //----------------------------------------------------

    cycle_start() {

    //----------------------------------------------------

          this.interval = setInterval(this.query.bind(this), this.dt);

    }
    //----------------------------------------------------

    cycle_stop() {

    //----------------------------------------------------

        clearInterval(this.interval);

    }
    //----------------------------------------------------

    query() {

    //----------------------------------------------------

          this.request.open('GET', this.url, true);
          this.request.onreadystatechange = this.request_cb.bind(this);
          this.request.send(null);

    }
    //----------------------------------------------------

    request_cb(e) {

    //----------------------------------------------------

        if (this.request.readyState === 4) {
            if (this.request.status === 200) {

                this.response = this.request.responseText;
                //console.log(this.data);

                this.msg("LOAD",{'msg':this.url},4);
                this.done();
                return;

            }
            this.msg("ERROR",{'msg':this.url});
            this.has_success=false;
            this.done();
            return;
        }

    }
    //----------------------------------------------------
    onProcessResult(){
    //----------------------------------------------------
        this.msg("LOADED",{'msg':this.url}),7;
        if(this.format=="json"){
            this.response=JSON.parse (this.response);
        }
        else if(this.format=="xml"){

              var parser = new DOMParser();

              var xmlDoc = parser.parseFromString(this.response,"text/xml");
              this.response=xmlDoc.documentElement;
        }
        else if(this.format=="obj"){

              var parser = new DOMParser();

              var xmlDoc = parser.parseFromString(this.response,"text/xml");
              this.response=from_xml(null,xmlDoc.documentElement);
        }else {
            this.msg("ERROR_FORMAT",{'msg':this.url});
        }
        this.args["response"]=this.response;
    }
    //----------------------------------------------------


}
//========================================================




