//========================================================

class Bd_Action extends Bd_Object {

//========================================================


    //----------------------------------------------------
    call(data={}){
    //----------------------------------------------------

        this.msg("CALL START",data,5);
        return this.onCall(data);
        this.msg("CALL STOP",5);

    }
    //----------------------------------------------------
    onCall(data){
    //----------------------------------------------------
        return;

    }

    //----------------------------------------------------
    executeEvent(event,callback,data=null,r=false){
    //----------------------------------------------------
        this.msg("EVENT",event,4);
        var node = new Event_Handler(this,event,callback.bind(this),data,r);
        node.start();
        return node;
    }
    //----------------------------------------------------
    LoadFile(url,callback,format=null,dt=null,data=null){
    //----------------------------------------------------
        this.msg("EVENT",{'url':url},4);
        var node = new LoadFile(this,callback.bind(this),url,format,dt,data=data);
        node.start();
        return node;
    }
    //----------------------------------------------------
}
//========================================================

