


//========================================================

class Task extends Atom {

//========================================================
/*
gère l'execution des évènements de la classe parent
démarre l'execution avec la fonction start
appelle la fonction callback quand c'est fini'
*/
    //----------------------------------------------------
    constructor(parent,callback,args){
    //----------------------------------------------------

        super(parent,null);
        if(args==null){args={};}
        this.msg("EVENT_NEW",args,3);
        this.time_start=CLOCK.getTime();

        this.callback=callback;//fonction à appeler quand fini
        this.args=args;//arguments pour callback
        this.is_done=false;
        this.has_success=true;
    }

    //----------------------------------------------------
    start(){
    //----------------------------------------------------
        this.msg("EVENT_START",this.args,3);
        this.onStart();
        this.waitEnded();
    }
    //----------------------------------------------------
    onStart(){
    //----------------------------------------------------
            this.done();
    }
    //----------------------------------------------------
    done(){
    //----------------------------------------------------
        this.is_done= true;

    }
    //----------------------------------------------------
    is_finish(){
    //----------------------------------------------------
        return this.is_done;

    }

    //----------------------------------------------------
    waitEnded(){
    //----------------------------------------------------

        if(this.is_finish()==true){
            this.msg("EVENT_DONE",this.args,3);
            if(this.has_success==true){
                this.onProcessResult();
                this.callback(this.args);
            }
        }else{
             setTimeout(this.waitEnded.bind(this),DT);
        this.msg("EVENT_WAIT",{},2);
        }
    }
    //----------------------------------------------------
    onProcessResult(){
    //----------------------------------------------------

    }
    //----------------------------------------------------

}
//========================================================
