var DEBUG=false;
DEBUG=true;
var DEBUG_LEVEL=5;
var CLOCK = new Date();
var DT=100;
var ip="http://192.168.1.14";


function import_js(tag,url){

    //console.log(tag,url);
    jQuery('<script/>', {
        src: ip+url,
    }).appendTo(tag);
}


function import_css(tag,url){

    jQuery('<link/>', {
        href: ip+url,
        rel: "stylesheet",
    }).appendTo(tag);
}

function import_all(tag,js_lib_url){

    //import_js(tag,js_lib_url+"/settings.js");

    //abstract classes
    //==============================================

    import_js(tag,js_lib_url+"/core/xml.js");
    import_js(tag,js_lib_url+"/core/Base.js");
    import_js(tag,js_lib_url+"/core/Store.js");
    import_js(tag,js_lib_url+"/core/LinkStore.js");
    import_js(tag,js_lib_url+"/core/Atom.js");
    import_js(tag,js_lib_url+"/core/Link.js");
    import_js(tag,js_lib_url+"/core/Query.js");

    //root classes
    //==============================================

    import_js(tag,js_lib_url+"/base/Bd_Object.js");
    import_js(tag,js_lib_url+"/base/Bd_Data.js");
    import_js(tag,js_lib_url+"/base/Bd_Action.js");
    import_js(tag,js_lib_url+"/base/Bd_Main.js");
    import_js(tag,js_lib_url+"/base/Task.js");
    import_js(tag,js_lib_url+"/base/LoadFile.js");

    import_js(tag,js_lib_url+"/scripts/Do.js");
    import_js(tag,js_lib_url+"/scripts/ScriptNode.js");
    import_js(tag,js_lib_url+"/scripts/Script.js");
    import_js(tag,js_lib_url+"/scripts/Actions.js");
    import_js(tag,js_lib_url+"/scripts/Event_Handler.js");

    //main
    //==============================================

    import_js(tag,js_lib_url+"/Scene/SceneElement.js");
    import_js(tag,js_lib_url+"/Scene/SceneGroup.js");
    import_js(tag,js_lib_url+"/Scene/Scene.js");


    import_js(tag,js_lib_url+"/Scene/node/Call_Scene.js");
    import_js(tag,js_lib_url+"/Scene/node/ForChildren.js");
    import_js(tag,js_lib_url+"/Scene/node/SelectFirst.js");
    import_js(tag,js_lib_url+"/Scene/node/SelectPath.js");
    import_js(tag,js_lib_url+"/Scene/node/SelectXml.js");
    import_js(tag,js_lib_url+"/Scene/node/SelectXmlScript.js");
    import_js(tag,js_lib_url+"/Scene/node/TestAttr.js");
    import_js(tag,js_lib_url+"/Scene/node/Sequence.js");
    import_js(tag,js_lib_url+"/Scene/node/SelectLocal.js");

    import_js(tag,js_lib_url+"/Scene/html/HtmlHandler.js");
    import_js(tag,js_lib_url+"/Scene/html/Draw.js");
    import_js(tag,js_lib_url+"/Scene/html/HtmlAnimate.js");
    import_js(tag,js_lib_url+"/Scene/html/HtmlClass.js");
    import_js(tag,js_lib_url+"/Scene/html/HtmlCss.js");
    import_js(tag,js_lib_url+"/Scene/html/HtmlEffect.js");
    import_js(tag,js_lib_url+"/Scene/html/HtmlMethod.js");
    import_js(tag,js_lib_url+"/Scene/html/SelectPart.js");
    import_js(tag,js_lib_url+"/Scene/html/ExecHtml.js");


    import_js(tag,js_lib_url+"/SceneItems/Div.js");
    import_js(tag,js_lib_url+"/SceneItems/Part.js");
    import_js(tag,js_lib_url+"/SceneItems/Text.js");
    import_js(tag,js_lib_url+"/SceneItems/TextAttr.js");
    import_js(tag,js_lib_url+"/SceneItems/Media.js");
    import_js(tag,js_lib_url+"/SceneItems/Icon.js");
    import_js(tag,js_lib_url+"/SceneItems/Icon_Awesome.js");
    import_js(tag,js_lib_url+"/SceneItems/Icon_Text.js");
    import_js(tag,js_lib_url+"/SceneItems/Icon_Image.js");
    import_js(tag,js_lib_url+"/SceneItems/Layers.js");
    import_js(tag,js_lib_url+"/SceneItems/Layer.js");
    import_js(tag,js_lib_url+"/SceneItems/Slideshow.js");
    import_js(tag,js_lib_url+"/SceneItems/Curve.js");
    import_js(tag,js_lib_url+"/SceneItems/Curve3d.js");

    import_js(tag,js_lib_url+"/admin/SceneBar.js");

    //css
    //==============================================
    import_js(tag,"/__static/externals/w3.js");
    import_css(tag,"/__static/externals/w3.css");
    import_css(tag,"/__static/main.css");
    import_css(tag,"/__static/externals/fontawesome-free-5.11.2.min.css");
    import_js(tag,"/__static/externals/fontawesome-free-5.11.2.min.js");
    import_js(tag,"/__static/externals/vis.min.js");
    import_css(tag,"/__static/externals/vis.min.css");

    import_js(tag,"http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js");
    import_js(tag,"/__static/externals/vis-graph3d.min.js");

    import_js(tag,"/__static/externals/vis-network.min.js");
    import_css(tag,"/__static/externals/vis-network.min.css");

    import_js(tag,"/__static/externals/vis-timeline-graph2d.min.js");
    import_css(tag,"/__static/externals/vis-timeline-graph2d.min.css");

//
}





