
//========================================================

class Script extends ScriptNode {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        this.msg("SCRIPT_SETUP",{});
        this.add_variable("exec",data,"__main__");
        this.add_variable("select",null);
        this.add_variable("tag",null);
    }
    //----------------------------------------------------

    onSetupDone(){

    //----------------------------------------------------

        this.executeEvent("setup",this.onSetupEventDone,{},true);
    }
    //----------------------------------------------------

    onSetupEventDone(){

    //----------------------------------------------------
        this.setup();
        //this.make();

    }
    //----------------------------------------------------
    onCall(data){
    //----------------------------------------------------

        var node=this.search_path("__main__");
        console.log(node);
        if(node !=null){
            this.msg("SCRIPT_MAKE",{'node':node.path()});



            if(this.select!=null){
                data.selected=this.search_path(this.select);
            }else{
                data.selected=this.root().content;
            }
            data.div=$(this.tag);
            //console.log(node,data.div,data.selected);
            node.call(data);


        }else{
            //console.log(this.path(),"NO MAIN TO START");
        }


    }
    //----------------------------------------------------
     onMakeCallX(selected){
    //----------------------------------------------------
        var node=this.search_path("__main__");
        //node.executeEvent("call",this.onMakeDone.bind(this));
        //node.make(selected);
    }
    //----------------------------------------------------
    onMakeDone(data){
    //----------------------------------------------------
        this.msg("SCRIPT_DONE",data);
        this.executeEvent("cleanup",this.onCleanupDone,{},true);
    }
    //----------------------------------------------------
    onCleanupDone(){
    //----------------------------------------------------

    }
    //----------------------------------------------------
    call_part(name,data={}){
    //----------------------------------------------------

    return this.search_path(name).call(data);

    }
    //----------------------------------------------------

}
//========================================================



