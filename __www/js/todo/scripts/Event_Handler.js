


//========================================================

class Event_Handler extends Task {

//========================================================
/*
gère l'execution des évènements de la classe parent
démarre l'execution avec la fonction start
appelle la fonction callback quand c'est fini'
*/
    //----------------------------------------------------
    constructor(parent,event,callback,args,r=false){
    //----------------------------------------------------

        super(parent,callback,args);

        this.event=event;
        this.r=r;//recursif

        if( r==true ){
            this.actions= parent.all().by_class(Do).by_attr_value("event",event);

        }else{
            this.actions= parent.children.by_class(Do).by_attr_value("event",event);
        }
        //console.log(this.actions,this.actions.size());
        //parent.tree();
    }
    //----------------------------------------------------
    onStart(){
    //----------------------------------------------------

        this.actions.execute("reset");
        this.actions.execute("call",this.args);

    }
    //----------------------------------------------------
    is_finish(){
    //----------------------------------------------------
        //console.log(this.actions.by_attr_value("finish",false).size());
        if( this.actions.by_attr_value("finish",false).size()>0){
            this.done();
            return false;
        }else{
            return true;
        }
    }
    //----------------------------------------------------

}
//========================================================
