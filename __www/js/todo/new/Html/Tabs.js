import {HtmlElement} from './HtmlElement.js';

//======================================================
export class Tabs extends HtmlElement {
//======================================================
    //---------------------------------------------------
    make() {
    //---------------------------------------------------
        this.nodes=$("<div>");

        var header=this.onGetHeader();

        var lst=this.by_class(HtmlElement);
        for (const child in lst) {

            var elt=this.build_tab(lst[child],child);
            elt.appendTo(header);
        } 

        var content=this.onGetContent();

        var lst=this.by_class(HtmlElement);
        for (const child in lst) {

            var elt=this.build_child(lst[child],child);
            elt.appendTo(content);
        } 

        header.appendTo(this.nodes);
        content.appendTo(this.nodes);
        return this.nodes;
     }
    //---------------------------------------------------
    onGetHeader() {
    //---------------------------------------------------
        var node=$("<ul>")
                    .addClass("nav nav-tabs")
                    .attr("id","tabs_"+this.id);

        return node;
      }
    //---------------------------------------------------
    onGetContent() {
    //---------------------------------------------------
        var node=$("<div>")
                    .addClass("tab-content")
                    .attr("id","tabs_content_"+this.id);

        return node;
      }
    //---------------------------------------------------
    build_tab(child,i) {
    //---------------------------------------------------
        var child_button_id="tabs_"+child.id+"_button";
        var child_content_id="tabs_"+child.id+"_content";

        if( i == 0){
            var classes="nav-link active";
            var selected="true";
        }else{
            var classes="nav-link";
            var selected="false";
        }

        var node=$("<li>")
                    .attr("role","presentation" )
                    .addClass("nav-item");


        var button=$("<button>")
                    .text(child.data.title)
                    .addClass(classes)
                    .attr("type","button" )
                    .attr("role","tab" )
                    .attr("id",child_button_id )
                    .attr("data-bs-toggle","tab" )
                    .attr("data-bs-target","#"+child_content_id )
                    .attr("aria-controls",child_content_id )
                    .attr("aria-selected",selected )
                    .appendTo(node);

        return node;
      }
    //---------------------------------------------------
    build_child(child,i) {
    //---------------------------------------------------
        var child_button_id="tabs_"+child.id+"_button";
        var child_content_id="tabs_"+child.id+"_content";

        if( i == 0){
            var classes="tab-pane fade show active";
        }else{
            var classes="tab-pane fade";
        }


        var node=$("<div>")
                    .addClass(classes)
                    .attr("id",child_content_id )
                    .attr("role","tabpanel" )
                    .attr("aria-labelledby",child_button_id)
                    .attr("tabindex",i );



            var elt=child.make();
            elt.appendTo(node);

        return node;
      }
    //---------------------------------------------------
}
//======================================================

