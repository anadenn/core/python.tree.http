import {HtmlElement} from './HtmlElement.js';
  import {TreeNode} from './../Tree/TreeNode.js';
//======================================================
export class Accordion extends HtmlElement {
//======================================================
    //---------------------------------------------------
    make() {
    //---------------------------------------------------
        this.nodes=this.onGetNode();

        if(this.data.class){
           this.nodes.addClass(this.data.class);
        }


        var lst=this.by_class(HtmlElement);
        for (const child in lst) {

            var elt=this.build_child(lst[child]);
            elt.appendTo(this.nodes);
        } 
        return this.nodes;
     }
    //---------------------------------------------------
    onGetNode() {
    //---------------------------------------------------
        var node=$("<div>")
                    .addClass("accordion")
                    .attr("id","accordion_"+this.id);

        return node;
      }

    //---------------------------------------------------
    build_child(child) {
    //---------------------------------------------------
        var child_button_id="accordion_button_"+child.id;
        var child_content_id="accordion_content_"+child.id;

        var node=$("<div>")
                    .addClass("accordion-item");

        var header=$("<div>")
                    .addClass("accordion-header")
                    .attr("id",child_button_id)
                    .appendTo(node);

        var button=$("<button>")
                    .text(child.data.title)
                    .addClass("accordion-button collapsed")
                    .attr("type","button" )
                    .attr("data-bs-toggle","collapse"  )
                    .attr("data-bs-target","#"+child_content_id )
                    .attr("aria-expanded","false" )
                    .attr("aria-controls",child_content_id )
                    .appendTo(header);

        var content=$("<div>")
                    .addClass("accordion-collapse collapse")
                    .attr("id",child_content_id)
                    .attr("aria-labelledby",child_button_id)
                    .attr("data-bs-parent","#accordion_"+this.id)
                    .appendTo(node);

        var body=$("<div>")
                    .addClass("accordion-body")
                    .appendTo(content);

            var elt=child.make();
            elt.appendTo(body);

        return node;
      }
    //---------------------------------------------------
}
//======================================================

