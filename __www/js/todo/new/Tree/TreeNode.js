var COUNT=0;
//======================================================
function tree(root,level=0,max_level=3,children=3){
//======================================================
    if(level==max_level){return}
    var node=new TreeNode(root,{});
    for (let i = 0; i < children; i++) {
        tree(node,level+1,max_level,children);
    }
    return node;
}
//======================================================
export class TreeNode {
//======================================================

    //---------------------------------------------------
    constructor(parent=null, data={}) {
    //---------------------------------------------------
        //console.log("NEW",data);

        this.parent = null;
        this.data = data;
        this.children = {};
        this.id=COUNT;

        if(parent!=null){
            this.attach(parent);
        }

        if(!this.data["name"]){
            this.data["name"]="node_"+this.id;
        }

        this.onInit();
        COUNT+=1;
    }
   //---------------------------------------------------
    destroy() {
    //---------------------------------------------------
        this.onDestroy();
        this.detach();
    }
    //---------------------------------------------------
    onInit() {
    //---------------------------------------------------
        return;
    }
   //---------------------------------------------------
    onDestroy() {
    //---------------------------------------------------
        return;
    }
    //---------------------------------------------------
    onAttach(parent) {
    //---------------------------------------------------
        return;
    }
    //---------------------------------------------------
    onDetach(parent) {
    //---------------------------------------------------
        return;
    }
    //---------------------------------------------------
    attach(parent) {
    //---------------------------------------------------

        this.detach();
        parent.children[this.id]=this;
        this.parent=parent;
        this.onAttach(this.parent);
    }
    //---------------------------------------------------
    detach() {
    //---------------------------------------------------
        if ( this.parent != null){
            this.onDetach(this.parent);
            delete this.parent.children[this.id];
            this.parent=null;
        }
    }
    //---------------------------------------------------
    get_root() {
    //---------------------------------------------------
        var root=this;
        while (root.parent !=null) {
            root=root.parent;
        }
        return root;
    }
    //---------------------------------------------------
    get_all() {
    //---------------------------------------------------
        var result=[];
        result.push(this);
        for (const child in this.children) {
            var lst=this.children[child].get_all();
            for (const elt in lst ) {
                result.push(lst[elt]);
            } 
        } 
        return result;
    }
    //---------------------------------------------------
    by_class(cls) {
    //---------------------------------------------------
        var result=[];

        for (const child in this.children) {
        var node=this.children[child] ;
         if(node instanceof cls){
            result.push(node);
        }
        } 
        return result;
    }
    //---------------------------------------------------
    clear() {
    //---------------------------------------------------
        //var result=this.children.keys();

        for (const child in this.children) {
        var node=this.children[child] ;
         node.detach();
        } 

    }
    //----------------------------------------------------
    path(){
    //----------------------------------------------------

        if(this.parent){
            return this.parent.path()+this.data.name+"/";
        }else{
            return "/";
        }
        
    }
    //----------------------------------------------------
    child_by_name(name){
    //----------------------------------------------------
        var result=[];

        for(var i in this.children) {
            if( "name" in this.children[i].data ){
                if( this.children[i].data["name"]==name ){
                    return this.children[i]; 
                }
            }
        }




    }
    //----------------------------------------------------
    find(path){
    //----------------------------------------------------
        if(path[0]=="/"){
            var result=this.get_root();
        }else{
            var result=this;
        }

        var lst=path.split("/");

        for(var i in lst) {
            if( lst[i] ==".."){
                    result=result.parent;
            }else if( lst[i] !="." && lst[i] !=""){
                var node=result.child_by_name(lst[i]);
                if(  node ){
                    result=node;
                }else{
                    console.log("no path",this.path(),path);
                    return;
                }
            }
        }

        return result;

    }
    //----------------------------------------------------




}
//======================================================
