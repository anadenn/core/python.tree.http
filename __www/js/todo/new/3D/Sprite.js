  import {ThreeObject} from './ThreeObject.js';
  import * as THREE from '/seed-env/__static/threejs/three.module.js';
//======================================================
export class Sprite extends ThreeObject {
//======================================================


    //---------------------------------------------------
    onMake() {
    //---------------------------------------------------

	    this.imaSprite360 = new THREE.TextureLoader().load( './static/360.png' );
	    this.material360 = new THREE.SpriteMaterial( { map: this.imaSprite360, color: 0xffffff } );
	    this.sprite360 = new THREE.Sprite( this.material360 );
	    this.sprite360.userData = this.data.link;
	    this.sprite360.position.set( this.data.x, this.data.y, this.data.z );
	    this.sprite360.scale.set(5, 5);
	    this.sprite360.center.set( 0.5, 0 );
	    this.parent.groupe360.add( this.sprite360 );

        return;
  }
    //---------------------------------------------------
    onRun(time) {
    //---------------------------------------------------



    }
    //---------------------------------------------------
}
//======================================================
export class SpriteGroup extends ThreeObject {
//======================================================


    //---------------------------------------------------
    onMake() {
    //---------------------------------------------------

	    this.selectedObject360 = null;

	    this.groupe360 = new THREE.Group();
	    this.parent.scene.add( this.groupe360 );

        this.raycaster360 = new THREE.Raycaster();
        this.pointer360 = new THREE.Vector3();

        return;
  }
    //---------------------------------------------------
    onRun(time) {
    //---------------------------------------------------
		if ( this.selectedObject360 ) {
			this.selectedObject360.material.color.set( '#fff' );
			//this.selectedObject360.scale.set(6, 4.5, 0.2);
			this.selectedObject360.center.set( 0.5, 0 );
			this.selectedObject360 = null;
		}

		this.raycaster360.setFromCamera( this.pointer360, this.parent.camera );
		var intersects360 = this.raycaster360.intersectObject( this.groupe360, true );
        //console.log(intersects360.length);

		if ( intersects360.length > 0 ) {
			var res = intersects360.filter( function ( res ) {
				return res && res.object;
			} )[ 0 ];
			if ( res && res.object ) {
				this.selectedObject360 = res.object;
				//this.selectedObject360.scale.set(8, 6, 2);
				this.selectedObject360.center.set( 0.5, 0.25 );
				this.selectedObject360.material.color.set( '#cbe7c6' );
				//window.open(intersects[0].object.userData.URL);
			}
		}
    }
    //---------------------------------------------------
}
//======================================================




	
