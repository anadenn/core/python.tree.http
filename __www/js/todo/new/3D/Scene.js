  import {ThreeObject} from './ThreeObject.js';
  import {Object3D} from './Object3D.js';
  import * as THREE from '/seed-env/__static/threejs/three.module.js';
//======================================================
export class Scene extends ThreeObject {
//======================================================

    //---------------------------------------------------
    get_scene(){
    //---------------------------------------------------
        return this.scene;
    }
    //---------------------------------------------------
    onMake() {
    //---------------------------------------------------
        console.log(this);
 
        this.content=document.getElementById(this.data.select.substr(1));

        this.width=this.content.clientWidth;
        //this.height=this.content.clientHeight;
        //this.width=400;
        this.height=400;
        //console.log(this.content);
        window.scene=this;

        // scene
        this.scene = new THREE.Scene();
        this.renderer = new THREE.WebGLRenderer( { antialias: true } );

        this.renderer.setSize( this.width,  this.height );
        this.renderer.setAnimationLoop( this.run.bind(this) );
        this.content.appendChild( this.renderer.domElement );

        this.camera=null;
        //window.addEventListener('resize', this.resizeRendererToDisplaySize.bind(this));
        new ResizeSensor(jQuery(this.data.select), this.resizeRendererToDisplaySize.bind(this));
        var lst=this.by_class(Object3D);
        for (const child in lst) {

            var elt=lst[child].make();

        } 
        return;
  }
    //---------------------------------------------------
    run(time) {
    //---------------------------------------------------
        //console.log(this.camera);
        //console.log("test");

        var lst=this.by_class(ThreeObject);
        for (const child in lst) {
        var node=lst[child] ;
            node.run(time);
        } 
        this.renderer.render( this.scene, this.camera );
    }
    //---------------------------------------------------
    helpers() {
    //---------------------------------------------------
        //console.log("test");
        var elts=this.by_class(Object3D);
        for (const child in elts) {
            console.log(elts[child]);
            elts[child].helpers() ;

        } 
    }
    //---------------------------------------------------
    resizeRendererToDisplaySize() {
    //---------------------------------------------------
      this.width = this.content.clientWidth;
      //this.height = this.content.clientHeight;

        this.renderer.setSize(this.width, this.height);
      this.camera.aspect = this.width / this.height;
      this.camera.updateProjectionMatrix();
        var canvas=this.renderer.domElement;
      canvas.width = this.width;// * ratio;
      canvas.height = this.height;// * ratio;
      canvas.style.width = `${this.width}px`;
      canvas.style.height = `${this.height}px`

    }
    //---------------------------------------------------
}
//======================================================
