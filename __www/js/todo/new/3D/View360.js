  import {Object3D} from './Object3D.js';
  import {Sphere} from './Helpers.js';
  import * as THREE from '/seed-env/__static/threejs/three.module.js';
//======================================================
export class View360 extends Object3D {
//======================================================


    //---------------------------------------------------
    onBuild3d() {
    //---------------------------------------------------

	    this.geometry = new THREE.SphereGeometry(this.data.r, 60, 60);
        this.texture = new THREE.TextureLoader().load(this.data.url);
        this.texture.wrapS = THREE.RepeatWrapping;
        this.texture.repeat.x = -1;
        this.material = new THREE.MeshBasicMaterial({map: this.texture,side: THREE.DoubleSide});
        //this.material = new THREE.MeshNormalMaterial({normalMap: this.texture,bumpMap: this.texture,side: THREE.DoubleSide});
        //this.material = new THREE.MeshNormalMaterial({normalMap: this.texture,side: THREE.DoubleSide})
        this.material.transparent = false;
        return new THREE.Mesh(this.geometry, this.material);
    }
    //---------------------------------------------------
    run(time){
    //---------------------------------------------------



    }

    //---------------------------------------------------
    load(texture_url){
    //---------------------------------------------------
            this.texture = new THREE.TextureLoader().load(texture_url);
            this.texture.wrapS = THREE.RepeatWrapping;
            this.texture.repeat.x = -1;
            this.material = new THREE.MeshBasicMaterial({map: this.texture,side: THREE.DoubleSide});
            this.material.transparent = false;
            this.object3d.material=this.material;
    }
    //---------------------------------------------------
    onDebugEnter() {
    //---------------------------------------------------
        console.log("test View360");
        this.helper=new Sphere(this,{r:0.95*this.data.r});
        this.helper.make();
    }
    //---------------------------------------------------
    onDebugExit() {
    //---------------------------------------------------

        this.helper.destroy();
    }
    //---------------------------------------------------
}
//======================================================
