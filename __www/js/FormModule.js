import { Module } from "./client.js";

//=========================================================
export class FormModule extends Module {
//=========================================================


    //-----------------------------------------------------
    update() {
    //-----------------------------------------------------
       var data=this.onRequest();
       console.log("send",data);
       this.parent.ask_json(data,this.onDo.bind(this));
        return false;
    }
    //-----------------------------------------------------
    onRequest() {
    //-----------------------------------------------------

        var data = {};
        const form = document.getElementById(this.id);
        for (let i = 0; i < form.elements.length; i++) {
          const element = form.elements[i];
          
          // Vérifier que l'élément a un nom et une valeur
          if (element.name && element.value) {
            // Ajouter le champ à l'objet
            data[element.name] = element.value;
          }
        }
        return data;
    }
    //-----------------------------------------------------
    onDo(data) {
    //-----------------------------------------------------
       console.log("result",data);
    }
    //-----------------------------------------------------
}
//=========================================================
