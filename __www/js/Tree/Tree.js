import { Store } from "./Store.js";
import { LinkStore } from "./LinkStore.js";

//compteur d'élément
var COUNT=0;

//========================================================

export class Tree{

//========================================================
/*
    crée une arborescence parent/enfants pour gerer les objets
    utiliser la classe par héritage pour creer différents objets
    imbricables
*/

    //----------------------------------------------------
    constructor(parent, name){
    //----------------------------------------------------

        this.msg("NEW",{},1);
        if( name) {
            this.name = decodeURI(name);
        }else{
            this.name = "NODE_"+COUNT;
            COUNT=COUNT+1;
        }
        //console.log("new",this.constructor.name,name);

        this.parent = null;
        this.children = new Store();
        this.inputs =  new LinkStore();
        this.outputs = new LinkStore();

        this.attach(parent);
    }
    //----------------------------------------------------
    destroy(){
    //----------------------------------------------------
        //console.log("destroy",this.path());
        this.msg("DESTROY",{},1);
        this.inputs.destroy();
        this.outputs.destroy();
        this.children.destroy();

        this.detach();

    }
    //----------------------------------------------------
    getClass(){
    //----------------------------------------------------
        return this.constructor.name;
    }
    //----------------------------------------------------
    attach(parent){
    //----------------------------------------------------
        this.detach();
        if( parent) {
            this.parent = parent;
            parent.children.append(this);
        }
    }
    //----------------------------------------------------
    detach(){
    //----------------------------------------------------
        if (this.parent){
            this.parent.children.remove(this);
            this.parent=null;
        }
    }


    //----------------------------------------------------
    root(){
    //----------------------------------------------------
    //Renvoie la racine de l’arbre

        if(this.parent){
            return this.parent.root();
        }else{
            return this;
        }
    }
    //----------------------------------------------------
    path(){
    //----------------------------------------------------

        if(this.parent){
            return this.parent.path()+"/"+this.name;
        }else{
            return "";
        }
        
    }
    //----------------------------------------------------
    query(path,action,...args){
    //----------------------------------------------------
       var node=this.search_path(path);
        return node[action](...args);
       
    }

    //----------------------------------------------------
    all(limit=null){
    //----------------------------------------------------
        var result=new Store();
        result.append(this);
        var lst= this.children.sorted();

        if( limit==null ){

            for(var i in lst) {
                result.append(lst[i].all()); 
            }
        }else if(limit>0 ){
            for(var i in lst) {
                result.append(lst[i].all(limit=limit-1)); 
            }
        }

        return result;

    }
    //----------------------------------------------------
    by_class(cls){
    //----------------------------------------------------

        return this.all().by_class(cls);
    }
    //----------------------------------------------------
    by_attr(...args){
    //----------------------------------------------------

        return this.all().by_attr(...args);
    }
 
    //----------------------------------------------------
    child_by_name(name){
    //----------------------------------------------------
        return this.children.by_attr_value("name",name).first();

    }
    //----------------------------------------------------
    search_path(path){
    //----------------------------------------------------
        if(path[0]=="/"){
            var result=this.root();
        }else{
            var result=this;
        }

        var lst=path.split("/");

        for(var i in lst) {
            if( lst[i] ==".."){
                    result=result.parent;
            }else if( lst[i] !="." && lst[i] !=""){
                var node=result.child_by_name(lst[i]);
                if(  node ){
                    result=node;
                }else{
                    console.log("no path",this.path(),path);
                    return;
                }
            }
        }

        return result;

    }
    //----------------------------------------------------
    create_path(path,cls){
    //----------------------------------------------------
        //console.log("create path",path);
        if(path[0]=="/"){
            var result=this.root();
        }else{
            var result=this;
        }

        var lst=path.split("/");

        for(var i in lst) {

            if(lst[i] !="" && lst[i] !="." ){
                result=result.get_child(lst[i],cls);
            }
        }
        //console.log("create path",result);
        return result;

    }
    //----------------------------------------------------
    get_child(name,cls,...args){
    //----------------------------------------------------
        //console.log("get_child",name);
        var node=this.child_by_name(name);
        if(  node ){
            var result=node;
        }else{
            var result=new cls(this,name,...args) ;
        }


        return result;

    }
   //----------------------------------------------------
    links(limit=null){
    //----------------------------------------------------
        var result=new LinkStore();
        result.append(this.outputs);

        if( limit==null ){
            var lst= this.children.iter();
            for(var i in lst) {
                result.append(lst[i].links()); 
            }
        }else if(limit>0 ){
            var lst= this.children.iter();
            for(var i in this.children) {
                result.append(this.children[i].links(limit=limit-1)); 
            }
        }

        return result;

    }
    //----------------------------------------------------
    links_by_tag(tag,...args){
    //----------------------------------------------------
        var result=new LinkStore();
        var links=this.all_links(...args);

        for(var i in links) {
            if( links[i].tag==tag ){
                result.append(links[i]); 
            }
        }

        return result;

    }


    //----------------------------------------------------
    objects(){
    //----------------------------------------------------

        return this.outputs.objects();
    }
    //----------------------------------------------------
    subjects(tag=null){
    //----------------------------------------------------
        return this.inputs.subjects();
    }
    //----------------------------------------------------
    link(list,tag=null,cls=Link){
    //----------------------------------------------------

        for(var obj in list) {
            new cls(this,obj,tag=tag);
        }
    }
    //----------------------------------------------------
    print(){
    //----------------------------------------------------
        var nodes=this.all().elements;
        for(var i in nodes ) {
            //console.log(nodes[i].path());
        }
    }
    //----------------------------------------------------
    msg(tag,data={},level=9){
    //----------------------------------------------------
        //if ( DEBUG && level > DEBUG_LEVEL ) {
            var sep="  -  ";
            var l0=15;
            var string=tag;
            if (string.length>l0){
                string=string.slice(0,l0);
            }else{
                string=string+" ".repeat(l0-string.length);
            }
            string=sep+string+sep;
            //console.log(level,string,this.getClass(),this.path(),data);
        //}

    }
    //----------------------------------------------------
    tree(level=1){
    //----------------------------------------------------

            var sep="   | ";
   
            var string=sep.repeat(level)+this.getClass()+" - "+this.name;
            console.log(string);
            this.children.execute('tree',level+1)


    }
    //----------------------------------------------------
}
//========================================================
