#--------------------------------------------------
function SERVER(){
#--------------------------------------------------

    # fonction d'initialisation des paramètres serveur

    PARAGRAPH "initialisation du serveur"

    # variables par défaut

    PARAGRAPH2 "default variable"

    GLOBAL "SERVER_IP" "localhost"
    GLOBAL "SERVER_PORT" "8000"
    GLOBAL "SERVER_PID" "$MAIN_PATH/.pid"

    # tente d'importer le fichier conf local "./.local"

    PARAGRAPH2 "import des variables à partir du fichier $MAIN_PATH/.local"
    GLOBAL_FILE "$MAIN_PATH/.local" "SERVER"
}
#--------------------------------------------------
function SERVER_CONF(){
#--------------------------------------------------
    echo "IP=$1" > "$MAIN_PATH/.local"
    echo "PORT=$2" >> "$MAIN_PATH/.local"
}
#--------------------------------------------------
function SERVER_CHECK(){
#--------------------------------------------------
    DEBUG true
    # check permet d'afficher les différents 
    # éléments présents dans le système

    PARAGRAPH "server status"
    SERVER_STATUS

    PARAGRAPH "bin programs"
  
    for elt in $( find "$MAIN_PATH/__bin" -mindepth 1 -type f)
    do
        LIST "$elt"
    done
  
    PARAGRAPH "bash execution libraries"
  
    for elt in $( find "$MAIN_PATH/__bash" -mindepth 1 -type f -name "*.sh")
    do
        LIST "$elt"
    done

    echo
    PARAGRAPH "bash install libraries"
  
    for elt in $( find "$MAIN_PATH/__install" -mindepth 1 -type f -name "*.sh")
    do
        LIST "$elt"
    done

    echo
}
#--------------------------------------------------
function SERVER_INSTALL(){
#--------------------------------------------------

    if [ ! -z "$1" ] && [ -d "$MAIN_PATH/workshops/$1" ]
    then

        if [ -f "$MAIN_PATH/workshops/$1/__install__.sh" ]
        then
            for elt in $( find "$MAIN/__install" -mindepth 1 -type f -name "*.sh")
            do
                source "$elt"
            done

            echo "$MAIN_PATH/workshops/$1/__install__.sh"
        fi

    else
        PARAGRAPH2 "choose workshop"

        for elt in $( find "$MAIN_PATH/workshops" -mindepth 1 -maxdepth 1 -type d)
        do
            LIST "$(basename $elt)"
        done
    fi

}
#--------------------------------------------------
function SERVER_START(){
#--------------------------------------------------
    if [ ! -f "$SERVER_PID" ]
    then
        MESSAGE "starting server"
        @python-http "ip=$SERVER_IP" "port=$SERVER_PORT" &
        pid=$!
        echo "$pid" >  "$SERVER_PID"
    else
        MESSAGE "server already running"
    fi
}
#--------------------------------------------------
function SERVER_STOP(){
#--------------------------------------------------
    if [ -f "$SERVER_PID" ]
    then
        MESSAGE "stoping server"
        pid="$(cat $SERVER_PID)"
        curl -X POST http://$SERVER_IP:$SERVER_PORT/stop
        rm "$SERVER_PID"

    else
        MESSAGE "server already stopped"
    fi
}
#--------------------------------------------------
function SERVER_STATUS(){
#--------------------------------------------------
    if [ -f "$SERVER_PID" ]
    then
        MESSAGE "server is running"

    else
        MESSAGE "server stopped"
    fi
}
#--------------------------------------------------
function SERVER_EXEC(){
#--------------------------------------------------

    if [ -f "$1" ]
    then
        MESSAGE "exec $@"
        source "$1"
    else
        MESSAGE "$1 is not a script"
    fi
}
#--------------------------------------------------
function SERVER_GET(){
#--------------------------------------------------
    MESSAGE "GET $@"
    curl -X GET http://$SERVER_IP:$SERVER_PORT$@
}
#--------------------------------------------------
function SERVER_POST(){
#--------------------------------------------------
    MESSAGE "POST $@"
curl --header "Content-Type: application/json" \
  --request POST \
  --data "$(cat)" \
  http://$SERVER_IP:$SERVER_PORT$@

}
#--------------------------------------------------
